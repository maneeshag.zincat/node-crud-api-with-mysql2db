const express = require('express'),
    app = express(),
    bodyParser = require('body-parser');
require('express-async-errors')
const cors = require('cors')
const db = require('./db'),
    customerRoutes = require('./controllers/customer_controller')

app.use(cors())
//middleware
app.use(bodyParser.json())
app.use('/api/customers', customerRoutes)
app.use((err, req, res, next) => {
    console.log(err)
    res.status(err.statusMessage || 500).send('Something went wrong!')
})

db.query("SELECT 1")
    .then(() => {
        console.log('db connection  succeeded.')
        app.listen(4000,
            () => console.log('server started at 4000'))
    })
    .catch(err => console.log('db connection failed. \n' + err))
