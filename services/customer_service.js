const db = require("../db");

//get all customers
module.exports.getAllCustomers = async () => {
  const [records] = await db.query("SELECT * FROM mydb.Customer");
  return records;
};

//get customer by id
module.exports.getCustomerByID = async (id) => {
  const [[record]] = await db.query(
    "SELECT * FROM mydb.Customer WHERE id = ?",
    [id]
  );
  return record;
};

//get all customer types
module.exports.getAllCustomerTypes = async () => {
  const [records] = await db.query("SELECT * FROM mydb.customer_type");
  return records;
};

//get all counties
module.exports.getAllCountryTypes = async () => {
  const [records] = await db.query("SELECT * FROM mydb.country");
  return records;
};

//delete customer
module.exports.deleteCustomer = async (customer_id) => {
  const [{ affectedRows }] = await db.query(
    "DELETE FROM mydb.Customer WHERE customer_id = ?",[customer_id]
  );
  return affectedRows;
};
