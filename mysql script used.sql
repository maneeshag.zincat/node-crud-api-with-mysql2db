CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8;
USE `mydb`;

-- Create the `Customer` table
CREATE TABLE IF NOT EXISTS `mydb`.`Customer`
(
    `customer_id`     INT AUTO_INCREMENT NOT NULL,
    `customer_code`   VARCHAR(45)        NULL,
    `company_name`    VARCHAR(45)        NULL,
    `reference_no`    VARCHAR(45)        NULL,
    `customer_type`   VARCHAR(45)        NULL,
    `customer_name`   VARCHAR(45)        NULL,
    `passport`        VARCHAR(45)        NULL,
    `billing_address` VARCHAR(45)        NULL,
    `mobile_number`   VARCHAR(45)        NULL,
    `email`           VARCHAR(45)        NULL,
    `country`         VARCHAR(45)        NULL,
    `gender`          VARCHAR(45)        NULL,
    `city`            VARCHAR(45)        NULL,
    `id`              VARCHAR(45)        NULL,
    PRIMARY KEY (`customer_id`)
) ENGINE = InnoDB;

INSERT INTO mydb.Customer (customer_code, company_name, reference_no, customer_type, customer_name, passport,
                           billing_address, mobile_number, email, country, gender, city, id)
VALUES ('C00-001', 'ZINCAT', 'REF-001', 'CASH', 'Maneesha', 'P0001', 'B0001', '071-9087721', 'manee@gmail.com',
        'Sri-Lanka', 'Male', 'Galle', 'I00-001'),
       ('C00-002', 'ZINCAT', 'REF-002', 'CASH', 'Dini', 'P0002', 'B0002', '071-9127721', 'dini@gmail.com', 'Sri-Lanka',
        'Female', 'Galle', 'I00-002');


CREATE TABLE `binddata`
(
    `id`   INT NOT NULL AUTO_INCREMENT,
    `data` BLOB,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;


-- Create the `contact_person_detail` table
CREATE TABLE IF NOT EXISTS `mydb`.`contact_person_detail`
(
    `contact_person_detail_id` INT AUTO_INCREMENT,
    `customer_name`            VARCHAR(45) NULL,
    `designation`              VARCHAR(45) NULL,
    `mobile_number`            VARCHAR(45) NULL,
    `email`                    VARCHAR(45) NULL,
    `customer_customer_id`     INT         NOT NULL,
    PRIMARY KEY (`contact_person_detail_id`),
    CONSTRAINT `fk_contact_person_detail_Customer1`
        FOREIGN KEY (`customer_customer_id`)
            REFERENCES `mydb`.`Customer` (`customer_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
) ENGINE = InnoDB;

-- Create the `customer_type` table
CREATE TABLE IF NOT EXISTS `mydb`.`customer_type`
(
    `type_id` INT         NOT NULL,
    `type`    VARCHAR(45) NULL
) ENGINE = InnoDB;

INSERT INTO mydb.customer_type (type_id, type)
VALUES (1, 'CASH');
INSERT INTO mydb.customer_type (type_id, type)
VALUES (2, 'CREDIT');

CREATE TABLE IF NOT EXISTS `mydb`.`country`
(
    `country_code` INT AUTO_INCREMENT PRIMARY KEY,
    `country`      VARCHAR(45) NULL
) ENGINE = InnoDB;

INSERT INTO mydb.country (country) VALUE ('SL');
INSERT INTO mydb.country (country) VALUE ('UQE');
INSERT INTO mydb.country (country) VALUE ('US');