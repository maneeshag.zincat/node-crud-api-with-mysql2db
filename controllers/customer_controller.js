const express = require('express');
const router = express.Router();
const db = require('../db');
const service = require('../services/customer_service');

// TODO public path - http://localhost:3000/api/customers */

router.get('/load-all-customers', async (req, res) => {
    console.log("load-all-customers API Called")
    const customers = await service.getAllCustomers();
    console.log("customer data return ",customers.length)
    res.send(customers);
    res.end();
});

//TODO load all customer types
router.get('/load-all-customer-types', async (req, res) => {
    console.log("load-all-customer-types API called")
    const types = await service.getAllCustomerTypes();
    console.log("types data return",types)
    res.send(types);
});

//TODO load all countries
router.get('/load-all-countries', async (req, res) => {
    console.log("load-all-countries API called")
    const country = await service.getAllCountryTypes();
    console.log("countries data return",country)
    res.send(country);
});




//TODO get customer by id
/** http://localhost:3000/api/customers/get-customer-by/:id */
router.get('/get-customer-by/:id', async (req, res) => {
    const customer = await service.getCustomerByID(req.params.id);
    if (!customer)
        res.status(404).json('No record with the given id: ' + req.params.id);
    else
        res.send(customer);
});

//TODO get customer by id
/** http://localhost:3000/api/customers/delete-customer/:id */
router.delete('/delete-customer/:customer_id', async (req, res) => {
    const affectedRows = await service.deleteCustomer(req.params.customer_id);
    if (affectedRows === 0)
        res.status(404).json('No record with the given id: ' + req.params.customer_id);
    else
        res.send('Deleted successfully.');
});

//http://localhost:3000/api/customers/save-customer
//TODO save customer function

const saveQuery =
    'INSERT INTO mydb.Customer (customer_id, customer_code, company_name, reference_no, customer_type, customer_name, passport, billing_address, mobile_number, email, country, gender, city, id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
const saveQueryTwo =
    'INSERT INTO mydb.contact_person_detail (contact_person_detail_id, customer_name, designation, mobile_number, email, customer_customer_id) VALUES (?, ?, ?, ?, ?, ?)';


//TODO customer save
router.post('/save-customer', async (req, res) => {
    const {
        customer_id,
        customer_code,
        company_name,
        reference_no,
        customer_type,
        customer_name,
        passport,
        billing_address,
        mobile_number,
        email,
        country,
        gender,
        city,
        id,
    } = req.body;

    try {
        await new Promise((resolve, reject) => {
            db.query(
                saveQuery,
                [
                    customer_id,
                    customer_code,
                    company_name,
                    reference_no,
                    customer_type,
                    customer_name,
                    passport,
                    billing_address,
                    mobile_number,
                    email,
                    country,
                    gender,
                    city,
                    id,
                ],
                (err, result) => {
                    if (err) {
                        console.error('An error has occurred:', err);
                        reject(err);
                    } else {
                        console.log('Data saved successfully:', result);
                        resolve();
                    }
                }
            );
        });

        await new Promise((resolve, reject) => {
            db.query(
                saveQueryTwo,
                [customer_name, 'sample', mobile_number, email, customer_id, customer_id],
                (err, result) => {
                    if (err) {
                        console.error('An error has occurred:', err);
                        reject(err);
                    } else {
                        console.log('Data saved successfully:', result);
                        resolve();
                    }
                }
            );
        });

        res.status(200).send('Data saved successfully');
    } catch (err) {
        console.error('An error has occurred:', err);
        res.status(500).send('An error has occurred: ' + err.message);
    }
});


module.exports = router;
